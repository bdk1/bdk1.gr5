package org.example;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {

        Scanner skaner = new Scanner(System.in);
        boolean isNotCorrect = true;
        String operator;
        double n2;

        do {
            try {
                System.out.println("Podaj pierwszą liczbę :");
                double n1 = skaner.nextDouble();
                System.out.println("Podaj operator działania :");
                operator = skaner.next();

                switch (operator) {
                    case "+":
                        System.out.println("Podaj drugą liczbę :");
                        n2 = skaner.nextDouble();
                        add(n1, n2);
                        isNotCorrect = false;
                        break;
                    case "-":
                        System.out.println("Podaj drugą liczbę :");
                        n2 = skaner.nextDouble();
                        sub(n1, n2);
                        isNotCorrect = false;
                        break;
                    case "*":
                        System.out.println("Podaj drugą liczbę :");
                        n2 = skaner.nextDouble();
                        multi(n1, n2);
                        isNotCorrect = false;
                        break;
                    case "/":
                        System.out.println("Podaj drugą liczbę :");
                        n2 = skaner.nextDouble();
                        if (n2 == 0) {
                            System.err.println("Dzielenie przez 0!");
                        } else {
                            div(n1, n2);
                            isNotCorrect = false;
                        }
                        break;
                    default:
                        System.err.println("Należy podać operator! Dostępne operatory działania to:\n+    -    *    /");
                }
            } catch (InputMismatchException ime) {
                System.err.println("Należy podać liczbę!");
                skaner.nextLine();
            }
        } while (isNotCorrect);
        skaner.close();

    }

    public static double add(double n1, double n2) {
        System.out.println("Wynik działania   " + n1 + " " + "+" + " " + n2 + " " + "=" + " " + (n1 + n2));
        return (n1 + n2);
    }

    public static double sub(double n1, double n2) {
        System.out.println("Wynik działania   " + n1 + " " + "-" + " " + n2 + " " + "=" + " " + (n1 - n2));
        return (n1 - n2);
    }

    public static double multi(double n1, double n2) {
        System.out.println("Wynik działania   " + n1 + " " + "*" + " " + n2 + " " + "=" + " " + (n1 * n2));
        return (n1 * n2);
    }

    public static double div(double n1, double n2) {
        System.out.println("Wynik działania   " + n1 + " " + "/" + " " + n2 + " " + "=" + " " + (n1 / n2));
        return (n1 / n2);
    }

}

