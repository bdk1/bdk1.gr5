package org.example;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;


public class AppTest {
    @Test
    public void addIsOKMain() {
        String data = "6\n+\n6\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void subIsOKMain() {
        String data = "6\n-\n6\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void multiIsOKMain() {
        String data = "6\n*\n6\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void divIsOKMain() {
        String data = "6\n/\n6\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void divby0IsOKMain() {
        String data = "6\n/\n0\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void operatorIsNotOKMain() {
        String data = "6\ns\n2\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void Number1IsNotOKMain() {
        String data = "x\n+\n2\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
    @Test
    public void Number2IsNotOKMain() {
        String data = "4\n+\n=\n";
        InputStream stdin = System.in;
        try {
            System.setIn(new ByteArrayInputStream(data.getBytes()));
            App.main(new String[]{});
        } finally {
            System.setIn(stdin);
        }
    }
}
