class DependencyContext {

    constructor() {
        for(var i = 0; i < arguments.length; i++) {
            if(arguments[i] != null) {
                console.log("argument: ", arguments[i]);
                let properties = arguments[i];
                if(properties != null) {
                    let that = this;
                    Object.keys(properties.json).forEach(function(key) {
                        that.register(key, properties.json[key]);
                    });
                }
            }
        }
    }

    #map = {};

    register(key, service) {
        if(this.contains(key)) {
            throw {error: "Element exists already in map: " + key}
        }
        this.#map[key] = service;
    }

    get(key) {
        return this.#map[key];
    }

    contains(key) {
        return typeof this.#map[key] !== 'undefined';
    }

    remove(key) {
        delete this.#map[key];
    }

    size() {
        var count = 0;
        for (name in this.#map) {
            count++;
        }
        return count;
    }

    contextApply() {
        for(name in this.#map) {
            let fields = this.get(name).requiredFields;
            if(fields && fields.length != 0) {
                for(var i = 0; i < fields.length; i++) {
                    if(this.contains(fields[i])) {
                        this.get(name)[fields[i]] = this.get(fields[i]);
                    }
                }
            }

            if(typeof this.get(name).postConstruct == "function") {
                this.get(name).postConstruct();
            }
        }
        return this;
    }
}