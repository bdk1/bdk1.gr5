class Properties {

    json = {
        "canvasElementId" : "canvas",
        "htmlDocument" : new Document(),
        "canvas" : new Canvas(),
        "buffers" : new Buffers(),
        "texture" : new Texture(),
        "scene" : new Scene()
    };
}