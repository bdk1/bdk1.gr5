class Canvas {
    #WEB_GL = "webgl";

    constructor() {
        this.requiredFields = ["canvasElementId", "htmlDocument", "logger"];
        console.log("Canvas2 constructed");
    }

    postConstruct() {
        console.log("Canvas2 postConstructed");
        this.canvas = this.htmlDocument.getElementById(this.canvasElementId);
        this.gl = this.canvas.getContext(this.#WEB_GL);

        if(!this.gl) {
        throw new Exception("No GL context!");
        }
    }
}