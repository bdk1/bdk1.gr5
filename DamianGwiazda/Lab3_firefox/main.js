import DependencyContext    from "./dependency.invertion/DependencyContext.js";
import PropertiesContext    from "./dependency.invertion/PropertiesContext";
import PropertiesShader     from "./dependency.invertion/PropertiesShader.js";
import PropertiesVertex     from "./dependency.invertion/PropertiesVertex.js";

export default class main {

    constructor(name) {
        let dependencyContext = new DependencyContext(
            new PropertiesContext(),
            new PropertiesShader(),
            new PropertiesVertex()
        ).contextApply();

        let canvas = dependencyContext.get("canvas");
        let shader = dependencyContext.get("shader");
        let bufferService = dependencyContext.get("buffers");
        let textureService = dependencyContext.get('texture');
        let scene = dependencyContext.get("scene");
        
        let buffers = bufferService.getBuffers();
        let texture = textureService.loadTexture(canvas.gl, "cubetexture.png");
        let time = 0;
        function render(now) {
            now *= 0.001;  //convert to seconds
            const deltaTime = now - time;
            time = now;
            scene.drawScene(canvas.gl, shader.getProgramInfo(), buffers, texture, deltaTime);
            requestAnimationFrame(render);
        }
        requestAnimationFrame(render);
    }
}

let main = new main();